# Begining

In this project, a home-made HMM model will be made according to many sources of formulas.

It is written by myself, by reading some papers and blogs.

One possible error when using might be rised because of the `precision` of float object. It is now type "float128" of python, but when goes beyond, the result might be wrong.

For viterbi algorithm, I highly recommand you to use its log2 version, which is viterbiL(). However, other functions might not have the log2 version, and that might be a problem.

# Import a model
Use `from hmm import HMM` to import the model into python, while the terminal is located at the project folder.

Also, you can use `from hmm import examModel` to import a model written for example.

# Initlize a model
You `always` need to use a exasted model to initlize it, weather you want to use it directly or you need to train one (talking later).

To initlize it, you need a `transfer matrix`, an `emission matrix`, and a `initial probility vector`.
    
The example below have state `A`, `B`, while the observation symbles are `L` and `M`.
    
For the transfer matrix, write it as a dictionary object. The example down here means a 2x2 matrix, which have two states that is A and B.
```python
{"AA":0.3, "AB":0.7, "BA":0.2, "BB":0.8}
```
        
For the emission matrix, like the one fermer, should be written as a dictionary object. The key of each item contains the state (the former) and the observation (the latter). Its value is the probility.
```python
{"AL":0.5, "AM":0.5, "BL":0.4, "BM":0.6}
```

For the initial probility vector, a dictionary object is alse requested. Key is the state symble, with its initial probility to be its value.
```python
{"A":0.5, "B":0.5}
```
      
All of the 3 parameters should be contained in a list. So, in the end, to initlize a HMM model, you should write like:
```python
hmm = HMM([{"AA":0.3, "AB":0.7, "BA":0.2, "BB":0.8}, {"AL":0.5, "AM":0.5, "BL":0.4, "BM":0.6}, {"A":0.5, "B":0.5}])
```

# Functions
After all things done abrove, you could do something with your model now.

        • HMM.jointProb(p, x)
            p is a state sequence, and x is a observation sequence. The function returns the probility of P(x | p, λ) * P(p | λ)
        • HMM.jointProbL(p, x)
            The value returned is log2 of jointProb(). Faster and less likely to be wrong due to the float number accuracy of Python than jointProb().
        • HMM.forwardProb(x)
            Probility of P(x | λ), in witch x is a observation sequence. Using forward algorithm.
        • HMM.backward(x)
            Probility of P(x | λ), in witch x is a observation sequence. Using backward algorithm.
        • HMM.viterbi(x)
            Return the most possible state sequence (second value) to produce the observation sequence x, with its probility (first value).
            The return looks like (0.0001, 'ABBBABBAA').
        • HMM.viterbiL(x)
            Log2 version of viterbi().
        • HMM.saveModel(output_file)
            Save the current model to a file. The file name is completely depend on the user, and should be written as a string, like "model.hmm" (with quotation).
        • HMM.loadModel(input_file)
            Load a model from a file.
`NOTE` that even if you want to `load a model`, you still need to use a useless (could be the examModel) but correct model to initlize a HMM object.
            
# Training

For some algorithm reason, you need a initial model to use the Baum-Welch algorithm to re-estimate parameters.

```python
HMM.unsupTrain(obe_seq_file, output_prob=False, output_prob_path="", threshold="0.0000001")
```

`obe_seq_file` is a path of the training data, which contains a observation sequence in one line.

`output_prob` is a switch for weather to output the probability each round into a file.

`output_prob_path` is the file path for output.

`threshold` is the threshold for stopping loop.

```python
HMM.unsupTrain_multi_seq_independence(obe_seq_file, output_prob=False, output_prob_path="", threshold="0.0000001")
```

`obe_seq_file` is a path of the training data, which contains many observation sequences, one sequence in one line.

`output_prob` is a switch for weather to output the probability each round into a file.

`output_prob_path` is the file path for output.

`threshold` is the threshold for stopping loop.
    
`NOTE` that the quality of the model you finally got is highly depend on your training data, including its quality and length, and partly the initial model.

# Referances

[1] Dugad R, Desai U B. A tutorial on hidden Markov models[J]. Signal Processing and Artifical Neural Networks Laboratory, Dept of Electrical Engineering, Indian Institute of Technology, Bombay Technical Report No.: SPANN-96.1, 1996.
    
[2] http://nbviewer.jupyter.org/gist/BenLangmead/7460513

[3] Larue P, Jallon P, Rivet B. Modified k-mean clustering method of HMM states for initialization of Baum-Welch training algorithm[C]//19th European Signal Processing Conference (EUSIPCO 2011). 2011: 951-955.

[4] Huggins-Daines D, Rudnicky A I. A constrained Baum-Welch algorithm for improved phoneme segmentation and efficient training[C]//Ninth International Conference on Spoken Language Processing. 2006.

[5] Miklós I, Meyer I M. A linear memory algorithm for Baum-Welch training[J]. BMC bioinformatics, 2005, 6(1): 231.

[6] Li X, Parizeau M, Plamondon R. Training hidden markov models with multiple observations-a combinatorial method[J]. IEEE Transactions on Pattern Analysis and Machine Intelligence, 2000, 22(4): 371-377.
