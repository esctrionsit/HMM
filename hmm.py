import math
import numpy
import decimal

#######################################################
##             !!! WARNING !!!                       ##
## Accuracy of calculation is not that good for now. ##
#######################################################

#################################################################
##                  !!! 2020.05.08 UPDATE !!!                  ##
## Decimal has now been added for better accuracy performance. ##
#################################################################

examModel = [{"FF":0.6, "FL":0.4, "LF":0.4, "LL":0.6}, {"FH":0.5, "FT":0.5, "LH":0.8, "LT":0.2}, {"F":0.5, "L":0.5}]

class HMM(object):

	def __init__(self, model):
		'''Initlize the HMM model user gaved, including transition, emission matrixes and initial probility vector.'''

		A, E, Pi = model[0], model[1], model[2]
		# self.Q for all state symbles
		# self.S for all observation symbles
		self.Q, self.S = set(), set()
		# add state symbles from parameter A to self.Q
		for a, prob in A.iteritems():
			self.Q.add(a[0])
			self.Q.add(a[1])
		# add state symbles and observation symbles from parameter E to self.Q and self.S
		for e, prob in E.iteritems():
			self.Q.add(e[0])
			self.S.add(e[1])

		self.Q = sorted(list(self.Q))
		self.S = sorted(list(self.S))
		#========================================================================================================
		# initlize symble-index map for Q and S
		self.qmap, self.smap = {}, {}
		for i in xrange(len(self.Q)):
			self.qmap[self.Q[i]] = i
		for i in xrange(len(self.S)):
			self.smap[self.S[i]] = i
		lenq = len(self.Q)
		#========================================================================================================
		# initlize transition matrix
		self.A = numpy.zeros(shape = (lenq, lenq), dtype = numpy.longdouble)
		for a, prob in A.iteritems():
			self.A[self.qmap[a[0]], self.qmap[a[1]]] = prob
		# make rows add to 1
		self.A /= self.A.sum(axis = 1)[:, numpy.newaxis]
		#========================================================================================================
		# initlize emission matrix
		self.E = numpy.zeros(shape = (lenq, len(self.S)), dtype = numpy.longdouble)
		for e, prob in E.iteritems():
			self.E[self.qmap[e[0]], self.smap[e[1]]] = prob
		# make rows add to 1
		self.E /= self.A.sum(axis = 1)[:, numpy.newaxis]
		#========================================================================================================
		# initlize initial probility vector
		self.Pi = [ 0.0 for i in range(0, len(self.Q))]
		for a, prob in Pi.iteritems():
			self.Pi[self.qmap[a]] = prob
		# make it add to 1
		self.Pi = numpy.divide(self.Pi, sum(self.Pi))
		#========================================================================================================
		# initlize log-base-2 version
		self.Alog = numpy.log2(self.A)
		self.Elog = numpy.log2(self.E)
		self.Pilog = numpy.log2(self.Pi)
		
	def jointProb(self, p, x):
		''' Return the probility, when observation string x is made by state string p, under the model initlized. \
		It is a simplified version of the first problem for HMM'''

		# transfer from symbles to indexes of self.Q and self.S
		p = map(self.qmap.get, p)
		x = map(self.smap.get, x)
		tot = decimal.Decimal(str(self.Pi[p[0]]))
		# probility for getting the state string p
		for i in xrange(1, len(p)):
			tot *= decimal.Decimal(str(self.A[p[i-1], p[i]]))
		# probility for getting the observation string x
		for i in xrange(0, len(p)):
			tot *= decimal.Decimal(str(self.E[p[i], x[i]]))

		return tot

	def jointProbL(self, p, x):
		''' Return the log2-ed probility, when observation string x is made by state string p, under the model initlized. \
		It is a simplified version of the first problem for HMM'''

		# transfer from symbles to indexes of self.Q and self.S
		p = map(self.qmap.get, p)
		x = map(self.smap.get, x)
		tot = self.Pilog[p[0]]
		# probility for getting the state string p
		for i in xrange(1, len(p)):
			tot += self.Alog[p[i-1], p[i]]
		# probility for getting the observation string x
		for i in xrange(0, len(p)):
			tot += self.Elog[p[i], x[i]]

		return tot

	def forwardProb(self, x):
		'''Return the probility, when observation string x is made by the model initlized. \
		Full version of the solution of the 1st problem for HMM, using Forward Procedure.'''
		x = map(self.smap.get, x)

		alpha = numpy.zeros(shape = (len(self.Q), len(x)), dtype = numpy.longdouble)
		for i in xrange(0, len(self.Q)):
			alpha[i, 0] = self.Pi[i] * self.E[i, x[0]]
		for j in xrange(1, len(x)):
			for i in xrange(0, len(self.Q)):
				sumval = 0
				for k in xrange(0, len(self.Q)):
					sumval += alpha[k, j-1] * self.A[k, i]
				alpha[i, j] = sumval * self.E[i, x[j]]
		
		tot = decimal.Decimal(0)
		for i in range(0, len(self.Q)):
			tot += decimal.Decimal(str(alpha[i, len(x)-1]))
		return tot

	def backwardProb(self, x):
		'''Return the probility, when observation string x is made by the model initlized. \
		Full version of the solution of the 1st problem for HMM, using Backward Procedure.'''
		x = map(self.smap.get, x)

		beta = numpy.zeros(shape = (len(self.Q), len(x)), dtype = numpy.longdouble)
		for i in xrange(0, len(self.Q)):
			beta[i, len(x)-1] = 1
		for j in xrange(len(x)-2, -1, -1):
			for i in xrange(0, len(self.Q)):
				sumval = 0
				for k in xrange(0, len(self.Q)):
					sumval += beta[k, j+1] * self.A[i, k] * self.E[k, x[j+1]]
				beta[i, j] = sumval

		tot = decimal.Decimal(str(0))
		for i in range(0, len(self.Q)):
			tot += decimal.Decimal(str(beta[i, 0] * self.Pi[i] * self.E[i, x[0]]))
		return tot

	def viterbi(self, x):
		'''Return the most probable state sequence of the given observation sequence, along with its probability.'''

		x = map(self.smap.get, x)
		nrow, ncol = len(self.Q), len(x)
		mat = numpy.zeros(shape = (nrow, ncol), dtype = numpy.longdouble)
		matTB =numpy.zeros(shape = (nrow, ncol), dtype = int)
		# Fill in first column
		for i in xrange(0, nrow):
			mat[i, 0] = self.E[i, x[0]] * self.Pi[i]
		# Fill in the rest of mat, and choose elements of matTB
		for j in xrange(1, ncol):
			for i in xrange(0, nrow):
				ep = self.E[i, x[j]]
				mx, mxi = mat[0, j-1] * self.A[0, i] * ep, 0
				for i2 in xrange(1, nrow):
					pr = mat[i2, j-1] * self.A[i2, i] * ep
					if pr > mx:
						mx, mxi = pr, i2
				mat[i, j], matTB[i, j] = mx, mxi
		# Find the final state which has the maximal probability
		omx, omxi = mat[0, ncol-1], 0
		for i in xrange(1, nrow):
			if mat[i, ncol-1] > omx:
				omx, omxi = mat[i, ncol-1], i
		# Trace for the state sequence
		i, p = omxi, [omxi]
		for j in xrange(ncol-1, 0, -1):
			i = matTB[i, j]
			p.append(i)
		p = ''.join(map(lambda x: self.Q[x], p[::-1]))

		return omx, p

	def viterbiL(self, x):
		'''Return the most probable state sequence of the given observation sequence, along with its log2-ed probability.'''

		x = map(self.smap.get, x)
		nrow, ncol = len(self.Q), len(x)
		mat = numpy.zeros(shape = (nrow, ncol), dtype = numpy.longdouble)
		matTB =numpy.zeros(shape = (nrow, ncol), dtype = int)
		# Fill in first column
		for i in xrange(0, nrow):
			mat[i, 0] = self.Elog[i, x[0]] + self.Pilog[i]
		# Fill in the rest of mat, and choose elements of matTB
		for j in xrange(1, ncol):
			for i in xrange(0, nrow):
				ep = self.Elog[i, x[j]]
				mx, mxi = mat[0, j-1] + self.Alog[0, i] + ep, 0
				for i2 in xrange(1, nrow):
					pr = mat[i2, j-1] + self.Alog[i2, i] + ep
					if pr > mx:
						mx, mxi = pr, i2
				mat[i, j], matTB[i, j] = mx, mxi
		# Find the final state which has the maximal probability
		omx, omxi = mat[0, ncol-1], 0
		for i in xrange(1, nrow):
			if mat[i, ncol-1] > omx:
				omx, omxi = mat[i, ncol-1], i
		# Trace for the state sequence
		i, p = omxi, [omxi]
		for j in xrange(ncol-1, 0, -1):
			i = matTB[i, j]
			p.append(i)
		p = ''.join(map(lambda x: self.Q[x], p[::-1]))

		return omx, p

	def unsupTrain(self, obe_seq_file, output_prob=False, output_prob_path="", threshold="0.0000001"):
		'''Train the model unsupervisedly using the given observation file. \
		Make sure the model is initlized, and a GOOD initial model can make the training better. '''
		f = open(obe_seq_file)
		line = f.readlines()
		f.close()
		O_seq = line[0].strip()
		x = map(self.smap.get, O_seq)
		print "training with" + O_seq

		#new_Pi = []
		new_A = numpy.zeros(shape = (len(self.Q), len(self.Q)), dtype = numpy.longdouble)
		new_E = numpy.zeros(shape = (len(self.Q), len(self.S)), dtype = numpy.longdouble)

		alpha = numpy.zeros(shape = (len(self.Q), len(x)), dtype = numpy.longdouble)
		for i in xrange(0, len(self.Q)):
			alpha[i, 0] = self.Pi[i] * self.E[i, x[0]]
		for j in xrange(1, len(x)):
			for i in xrange(0, len(self.Q)):
				sumval = 0
				for k in xrange(0, len(self.Q)):
					sumval += alpha[k, j-1] * self.A[k, i]
				alpha[i, j] = sumval * self.E[i, x[j]]

		beta = numpy.zeros(shape = (len(self.Q), len(x)), dtype = numpy.longdouble)
		for i in xrange(0, len(self.Q)):
			beta[i, len(x)-1] = 1
		for j in xrange(len(x)-2, -1, -1):
			for i in xrange(0, len(self.Q)):
				sumval = 0
				for k in xrange(0, len(self.Q)):
					sumval += beta[k, j+1] * self.A[i, k] * self.E[k, x[j+1]]
				beta[i, j] = sumval

		print "initlize new A..."
		for i in xrange(0, len(self.Q)):
			for j in xrange(0, len(self.Q)):
				a_numerator = 0
				a_division = 0
				for t in xrange(0, len(O_seq)-1):
					a_numerator += HMM.xi_train(self, alpha, beta, t, i, j, x[t+1])
					a_division += HMM.gamma_train(self, alpha, beta, t, i)
				new_A[i, j] = a_numerator / a_division
		print "initlize new E"
		for i in xrange(0, len(self.Q)):
			for j in xrange(0, len(self.S)):
				e_numerator = 0
				e_division = 0
				for t in xrange(0, len(O_seq)):
					if x[t] == j:
						e_numerator += HMM.gamma_train(self, alpha, beta, t, i)
					e_division += HMM.gamma_train(self, alpha, beta, t, i)
				new_E[i, j] = e_numerator / e_division

		new_A /= new_A.sum(axis = 1)[:, numpy.newaxis]
		new_E /= new_E.sum(axis = 1)[:, numpy.newaxis]

		old_change_Prob = decimal.Decimal(1)
		new_Prob = HMM.forwardProb_train(self, self.Q, self.S, new_A, new_E, self.Pi, self.qmap, self.smap, O_seq)
		old_Prob = HMM.forwardProb(self, O_seq)
		print "[new_Prob]   " + str(new_Prob)
		print "[old_Prob]   " + str(old_Prob) + "\n"
		if output_prob:
			f = open(output_prob_path, 'w+')
			print >> f, str(old_Prob)
			print >> f, str(new_Prob)
			f.close()
		change_Prob = new_Prob - old_Prob

		while change_Prob/old_change_Prob > decimal.Decimal(threshold):
			self.A = new_A
			self.E = new_E
			old_change_Prob = new_Prob

			alpha = numpy.zeros(shape = (len(self.Q), len(x)), dtype = numpy.longdouble)
			for i in xrange(0, len(self.Q)):
				alpha[i, 0] = self.Pi[i] * self.E[i, x[0]]
			for j in xrange(1, len(x)):
				for i in xrange(0, len(self.Q)):
					sumval = 0
					for k in xrange(0, len(self.Q)):
						sumval += alpha[k, j-1] * self.A[k, i]
					alpha[i, j] = sumval * self.E[i, x[j]]

			beta = numpy.zeros(shape = (len(self.Q), len(x)), dtype = numpy.longdouble)
			for i in xrange(0, len(self.Q)):
				beta[i, len(x)-1] = 1
			for j in xrange(len(x)-2, -1, -1):
				for i in xrange(0, len(self.Q)):
					sumval = 0
					for k in xrange(0, len(self.Q)):
						sumval += beta[k, j+1] * self.A[i, k] * self.E[k, x[j+1]]
					beta[i, j] = sumval

			new_A = numpy.zeros(shape = (len(self.Q), len(self.Q)), dtype = numpy.longdouble)
			new_E = numpy.zeros(shape = (len(self.Q), len(self.S)), dtype = numpy.longdouble)
			for i in xrange(0, len(self.Q)):
				for j in xrange(0, len(self.Q)):
					a_numerator = 0
					a_division = 0
					for t in xrange(0, len(O_seq)-1):
						a_numerator += HMM.xi_train(self, alpha, beta, t, i, j, x[t+1])
						a_division += HMM.gamma_train(self, alpha, beta, t, i)
					new_A[i, j] = a_numerator / a_division

			for i in xrange(0, len(self.Q)):
				for j in xrange(0, len(self.S)):
					e_numerator = 0
					e_division = 0
					for t in xrange(0, len(O_seq)-1):
						if x[t] == j:
							e_numerator += HMM.gamma_train(self, alpha, beta, t, i)
						e_division += HMM.gamma_train(self, alpha, beta, t, i)
					new_E[i, j] = e_numerator / e_division

			new_A /= new_A.sum(axis = 1)[:, numpy.newaxis]
			new_E /= new_E.sum(axis = 1)[:, numpy.newaxis]

			new_Prob = HMM.forwardProb_train(self, self.Q, self.S, new_A, new_E, self.Pi, self.qmap, self.smap, O_seq)
			old_Prob = HMM.forwardProb(self, O_seq)
			print "[new_Prob]   " + str(new_Prob)
			print "[old_Prob]   " + str(old_Prob) + "\n"
			if output_prob:
				f = open(output_prob_path, 'a')
				print >> f, str(new_Prob)
				f.close()
			change_Prob = new_Prob - old_Prob

		self.Alog = numpy.log2(self.A)
		self.Elog = numpy.log2(self.E)
		self.Pilog = numpy.log2(self.Pi)

	def unsupTrain_multi_seq_independence(self, obe_seq_file, output_prob=False, output_prob_path="", threshold="0.0000001"):
		'''Train the model unsupervisedly using the given observation file. \
		Make sure the model is initlized, and a GOOD initial model can make the training better. 
		Li X, Parizeau M, Plamondon R. Training hidden markov models with multiple observations-a combinatorial method[J]. IEEE Transactions on Pattern Analysis and Machine Intelligence, 2000, 22(4): 371-377.
		'''
		f = open(obe_seq_file)
		O_seqs = f.readlines()
		f.close()
		K = len(O_seqs)
		x = []
		for k in xrange(0, K):
			O_seqs[k] = O_seqs[k].strip()
			x.append(map(self.smap.get, O_seq[k]))
		print "training with:"
		print O_seqs
		print "\n"

		new_Pi = [0.0 for i in range(0, len(self.Q))]
		new_A = numpy.zeros(shape = (len(self.Q), len(self.Q)), dtype = numpy.longdouble)
		new_E = numpy.zeros(shape = (len(self.Q), len(self.S)), dtype = numpy.longdouble)

		print "initlize new Pi..."
		for i in xrange(0, len(self.Q)):
			Pi_child = 0
			for k in xrange(0, K):
				alpha = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
					for ii in xrange(0, len(self.Q)):
						alpha[ii, 0] = self.Pi[ii] * self.E[ii, x[k, 0]]
					for jj in xrange(1, len(x[k])):
						for ii in xrange(0, len(self.Q)):
							sumval = 0
							for tt in xrange(0, len(self.Q)):
								sumval += alpha[tt, jj-1] * self.A[tt, ii]
							alpha[ii, jj] = sumval * self.E[ii, x[k, jj]]

					beta = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
					for ii in xrange(0, len(self.Q)):
						beta[ii, len(x[k])-1] = 1
					for jj in xrange(len(x[k])-2, -1, -1):
						for ii in xrange(0, len(self.Q)):
							sumval = 0
							for k in xrange(0, len(self.Q)):
								sumval += beta[k, j+1] * self.A[ii, k] * self.E[k, x[k, jj+1]]
							beta[ii, jj] = sumval

				Pi_child += HMM.gamma_train(self, alpha, beta, 0, i)
			new_Pi[i] = Pi_child / K
		# make it add to 1
		new_Pi = numpy.divide(new_Pi, sum(new_Pi))

		print "initlize new A..."
		for i in xrange(0, len(self.Q)):
			for j in xrange(0, len(self.Q)):
				a_numerator = 0
				a_division = 0
				
				for k in xrange(0, K):
					alpha = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
					for ii in xrange(0, len(self.Q)):
						alpha[ii, 0] = self.Pi[ii] * self.E[ii, x[k, 0]]
					for jj in xrange(1, len(x[k])):
						for ii in xrange(0, len(self.Q)):
							sumval = 0
							for tt in xrange(0, len(self.Q)):
								sumval += alpha[tt, jj-1] * self.A[tt, ii]
							alpha[ii, jj] = sumval * self.E[ii, x[k, jj]]

					beta = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
					for ii in xrange(0, len(self.Q)):
						beta[ii, len(x[k])-1] = 1
					for jj in xrange(len(x[k])-2, -1, -1):
						for ii in xrange(0, len(self.Q)):
							sumval = 0
							for k in xrange(0, len(self.Q)):
								sumval += beta[k, j+1] * self.A[ii, k] * self.E[k, x[k, jj+1]]
							beta[ii, jj] = sumval

					a_numerator_child = 0
					a_division_child = 0
					for t in xrange(0, len(O_seq[k])-1):
						a_numerator_child += HMM.xi_train(self, alpha, beta, t, i, j, x[k, t+1])
						a_division_child += HMM.gamma_train(self, alpha, beta, t, i)
				a_numerator += a_numerator_child
				a_division += a_division_child
			new_A[i, j] = a_numerator / a_division
		print "initlize new E"
		for i in xrange(0, len(self.Q)):
			for j in xrange(0, len(self.S)):
				e_numerator = 0
				e_division = 0
				for k in xrange(0, K):
					alpha = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
					for ii in xrange(0, len(self.Q)):
						alpha[ii, 0] = self.Pi[ii] * self.E[ii, x[k, 0]]
					for jj in xrange(1, len(x[k])):
						for ii in xrange(0, len(self.Q)):
							sumval = 0
							for tt in xrange(0, len(self.Q)):
								sumval += alpha[tt, jj-1] * self.A[tt, ii]
							alpha[ii, jj] = sumval * self.E[ii, x[k, jj]]

					beta = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
					for ii in xrange(0, len(self.Q)):
						beta[ii, len(x[k])-1] = 1
					for jj in xrange(len(x[k])-2, -1, -1):
						for ii in xrange(0, len(self.Q)):
							sumval = 0
							for k in xrange(0, len(self.Q)):
								sumval += beta[k, j+1] * self.A[ii, k] * self.E[k, x[k, jj+1]]
							beta[ii, jj] = sumval

					e_numerator_child = 0
					e_division_cild = 0
					for t in xrange(0, len(O_seq[k])):
						if x[k, t] == j:
							e_numerator_child += HMM.gamma_train(self, alpha, beta, t, i)
						e_division_cild += HMM.gamma_train(self, alpha, beta, t, i)
					e_numerator += e_numerator_child
					e_division += e_division_cild
				new_E[i, j] = e_numerator / e_division

		new_A /= new_A.sum(axis = 1)[:, numpy.newaxis]
		new_E /= new_E.sum(axis = 1)[:, numpy.newaxis]

		old_change_Prob = decimal.Decimal(1)
		new_Prob = decimal.Decimal(1)
		old_Prob = decimal.Decimal(1)
		for k in xrange(0, K):		
			new_Prob *= HMM.forwardProb_train(self, self.Q, self.S, new_A, new_E, new_Pi, self.qmap, self.smap, O_seq[k])
			old_Prob *= HMM.forwardProb(self, O_seq[k])
		print "[new_Prob]   " + str(new_Prob)
		print "[old_Prob]   " + str(old_Prob) + "\n"
		if output_prob:
			f = open(output_prob_path, 'w+')
			print >> f, str(old_Prob)
			print >> f, str(new_Prob)
			f.close()
		change_Prob = new_Prob - old_Prob

		# iterate
		while change_Prob/old_change_Prob > decimal.Decimal(threshold):
			self.A = new_A
			self.E = new_E
			self.Pi = new_Pi
			old_change_Prob = change_Prob

			new_Pi = [0.0 for i in range(0, len(self.Q))]
			new_A = numpy.zeros(shape = (len(self.Q), len(self.Q)), dtype = numpy.longdouble)
			new_E = numpy.zeros(shape = (len(self.Q), len(self.S)), dtype = numpy.longdouble)

			print "initlize new Pi..."
			for i in xrange(0, len(self.Q)):
				Pi_child = 0
				for k in xrange(0, K):
					alpha = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
						for ii in xrange(0, len(self.Q)):
							alpha[ii, 0] = self.Pi[ii] * self.E[ii, x[k, 0]]
						for jj in xrange(1, len(x[k])):
							for ii in xrange(0, len(self.Q)):
								sumval = 0
								for tt in xrange(0, len(self.Q)):
									sumval += alpha[tt, jj-1] * self.A[tt, ii]
								alpha[ii, jj] = sumval * self.E[ii, x[k, jj]]

						beta = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
						for ii in xrange(0, len(self.Q)):
							beta[ii, len(x[k])-1] = 1
						for jj in xrange(len(x[k])-2, -1, -1):
							for ii in xrange(0, len(self.Q)):
								sumval = 0
								for k in xrange(0, len(self.Q)):
									sumval += beta[k, j+1] * self.A[ii, k] * self.E[k, x[k, jj+1]]
								beta[ii, jj] = sumval

					Pi_child += HMM.gamma_train(self, alpha, beta, 0, i)
				new_Pi[i] = Pi_child / K
			# make it add to 1
			new_Pi = numpy.divide(new_Pi, sum(new_Pi))

			print "initlize new A..."
			for i in xrange(0, len(self.Q)):
				for j in xrange(0, len(self.Q)):
					a_numerator = 0
					a_division = 0
					
					for k in xrange(0, K):
						alpha = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
						for ii in xrange(0, len(self.Q)):
							alpha[ii, 0] = self.Pi[ii] * self.E[ii, x[k, 0]]
						for jj in xrange(1, len(x[k])):
							for ii in xrange(0, len(self.Q)):
								sumval = 0
								for tt in xrange(0, len(self.Q)):
									sumval += alpha[tt, jj-1] * self.A[tt, ii]
								alpha[ii, jj] = sumval * self.E[ii, x[k, jj]]

						beta = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
						for ii in xrange(0, len(self.Q)):
							beta[ii, len(x[k])-1] = 1
						for jj in xrange(len(x[k])-2, -1, -1):
							for ii in xrange(0, len(self.Q)):
								sumval = 0
								for k in xrange(0, len(self.Q)):
									sumval += beta[k, j+1] * self.A[ii, k] * self.E[k, x[k, jj+1]]
								beta[ii, jj] = sumval

						a_numerator_child = 0
						a_division_child = 0
						for t in xrange(0, len(O_seq[k])-1):
							a_numerator_child += HMM.xi_train(self, alpha, beta, t, i, j, x[k, t+1])
							a_division_child += HMM.gamma_train(self, alpha, beta, t, i)
					a_numerator += a_numerator_child
					a_division += a_division_child
				new_A[i, j] = a_numerator / a_division
			print "initlize new E"
			for i in xrange(0, len(self.Q)):
				for j in xrange(0, len(self.S)):
					e_numerator = 0
					e_division = 0
					for k in xrange(0, K):
						alpha = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
						for ii in xrange(0, len(self.Q)):
							alpha[ii, 0] = self.Pi[ii] * self.E[ii, x[k, 0]]
						for jj in xrange(1, len(x[k])):
							for ii in xrange(0, len(self.Q)):
								sumval = 0
								for tt in xrange(0, len(self.Q)):
									sumval += alpha[tt, jj-1] * self.A[tt, ii]
								alpha[ii, jj] = sumval * self.E[ii, x[k, jj]]

						beta = numpy.zeros(shape = (len(self.Q), len(x[k])), dtype = numpy.longdouble)
						for ii in xrange(0, len(self.Q)):
							beta[ii, len(x[k])-1] = 1
						for jj in xrange(len(x[k])-2, -1, -1):
							for ii in xrange(0, len(self.Q)):
								sumval = 0
								for k in xrange(0, len(self.Q)):
									sumval += beta[k, j+1] * self.A[ii, k] * self.E[k, x[k, jj+1]]
								beta[ii, jj] = sumval

						e_numerator_child = 0
						e_division_cild = 0
						for t in xrange(0, len(O_seq[k])):
							if x[k, t] == j:
								e_numerator_child += HMM.gamma_train(self, alpha, beta, t, i)
							e_division_cild += HMM.gamma_train(self, alpha, beta, t, i)
						e_numerator += e_numerator_child
						e_division += e_division_cild
					new_E[i, j] = e_numerator / e_division

			new_A /= new_A.sum(axis = 1)[:, numpy.newaxis]
			new_E /= new_E.sum(axis = 1)[:, numpy.newaxis]

			old_change_Prob = decimal.Decimal(1)
			new_Prob = decimal.Decimal(1)
			old_Prob = decimal.Decimal(1)
			for k in xrange(0, K):		
				new_Prob *= HMM.forwardProb_train(self, self.Q, self.S, new_A, new_E, new_Pi, self.qmap, self.smap, O_seq[k])
				old_Prob *= HMM.forwardProb(self, O_seq[k])
			print "[new_Prob]   " + str(new_Prob)
			print "[old_Prob]   " + str(old_Prob) + "\n"
			if output_prob:
				f = open(output_prob_path, 'a')
				print >> f, str(new_Prob)
				f.close()
			change_Prob = new_Prob - old_Prob

		self.Alog = numpy.log2(self.A)
		self.Elog = numpy.log2(self.E)
		self.Pilog = numpy.log2(self.Pi)


	def gamma_train(self, alpha, beta, t, i):
		'''gamma without division'''
		return alpha[i, t] * beta[i, t]

	def xi_train(self, alpha, beta, t, i, j, o):
		'''probability of being in state i at time t and making a transition to state j at time j+1.  without division.'''
		return alpha[i, t] * self.A[i, j] * self.E[j, o] * beta[j, t+1]

	def forwardProb_train(self, Q, S, A, E, Pi, qmap, smap, x):
		'''Return the probility, when observation string x is made by the model initlized. \
		Full version of the solution of the 1st problem for HMM, using Forward Procedure.'''
		x = map(smap.get, x)

		alpha = numpy.zeros(shape = (len(Q), len(x)), dtype = numpy.longdouble)
		for i in xrange(0, len(Q)):
			alpha[i, 0] = Pi[i] * E[i, x[0]]
		for j in xrange(1, len(x)):
			for i in xrange(0, len(Q)):
				sumval = 0
				for k in xrange(0, len(Q)):
					sumval += alpha[k, j-1] * A[k, i]
				alpha[i, j] = sumval * E[i, x[j]]
		
		tot = decimal.Decimal(str(0))
		for i in range(0, len(Q)):
			tot += decimal.Decimal(str(alpha[i, len(x)-1]))
		return tot

	def saveModel(self, output_file):
		'''save current model as a file for easyer use'''
		string_Q = "\t".join(self.Q)
		string_S = "\t".join(self.S)
		Pi = []
		for i in self.Pi:
			Pi.append(str(i))
		string_Pi = "\t".join(Pi)
		string_A = ""
		for i in xrange(0, len(self.Q)):
			for j in range(0, len(self.Q)):
				string_A += str(self.A[i, j]) + "\t"
		string_E = ""
		for i in xrange(0, len(self.Q)):
			for j in range(0, len(self.S)):
				string_E += str(self.E[i, j]) + "\t"
		output_string = "\n".join([string_Q, string_S, string_Pi, string_A, string_E])
		f = open(output_file, 'w')
		f.write(output_string)
		f.close()

	def loadModel(self, input_file):
		'''load model from a file saved before'''
		f = open(input_file, 'r')
		lines = f.readlines()
		f.close()
		Q = lines[0].strip().split("\t")
		S = lines[1].strip().split("\t")
		Pi = lines[2].strip().split("\t")
		A = lines[3].strip().split("\t")
		E = lines[4].strip().split("\t")

		self.Q = Q
		self.S = S
		self.Pi = [ 0.0 for i in range(0, len(self.Q))]
		for i in xrange(0, len(Q)):
			self.Pi[i] = numpy.longdouble(Pi[i])
		self.A = numpy.zeros(shape = (len(self.Q), len(self.Q)), dtype = numpy.longdouble)
		for i in xrange(0, len(self.Q)):
			for j in xrange(0, len(self.Q)):
				self.A[i, j] = numpy.longdouble(A[i * len(self.Q) + j])
		self.A = numpy.zeros(shape = (len(self.Q), len(self.S)), dtype = numpy.longdouble)
		for i in xrange(0, len(self.Q)):
			for j in xrange(0, len(self.S)):
				self.A[i, j] = numpy.longdouble(A[i * len(self.S) + j])
		self.qmap, self.smap = {}, {}
		for i in xrange(len(self.Q)):
			self.qmap[self.Q[i]] = i
		for i in xrange(len(self.S)):
			self.smap[self.S[i]] = i

		self.Alog = numpy.log2(self.A)
		self.Elog = numpy.log2(self.E)
		self.Pilog = numpy.log2(self.Pi)



